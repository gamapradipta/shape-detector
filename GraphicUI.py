import os
import tkinter as tk
import tkinter.ttk as ttk
import time
from tkinter import *
from tkinter import filedialog
from tkinter.ttk import *
import threading
from PIL import ImageTk, Image
from tkinter import scrolledtext
import cv2
from image_processor import ImageProcessor
import numpy as np
from shape_processor import ShapeProcessor

ip = ImageProcessor()
sp = ShapeProcessor()

globalResult = []
globalFacts = []
globalHitRules = []

def sourceOnClick() :
    clearScrollText(matchedFacts)
    clearScrollText(detectionResult)
    clearScrollText(hitRules)
    path=tk.filedialog.askopenfilename(filetypes=[("Image File",'.jpg .png')])
    ip.setImageFile(''.join(path))
    sp.new(ip)
    sp.reset()
    img = Image.open(path)
    img = img.resize((500,500), Image.ANTIALIAS)
    tkimage = ImageTk.PhotoImage(img)
    sourceImageBox.configure(image=tkimage)
    sourceImageBox.photo = tkimage
    redrawn_image = ip.getImageMatrices()
    redrawn_image = ImageTk.PhotoImage(Image.fromarray(redrawn_image).resize((500,500), Image.ANTIALIAS))
    shapeImageBox.configure(image=redrawn_image)
    shapeImageBox.photo = redrawn_image
    print("updated")

def printIntoScrollText(scrolledText, listOfText) :
    scrolledText.config(state = tk.NORMAL)
    for text in listOfText:
        scrolledText.insert(INSERT, text)
        scrolledText.insert(END, '\n')
    scrolledText.config(state = tk.DISABLED)

def clearScrollText(scrolledText) :
    scrolledText.config(state = tk.NORMAL)
    scrolledText.delete(1.0,END)
    scrolledText.config(state = tk.DISABLED)

def selectItem(event) :
    curItem = tree.focus()
    sp.reset()
    found, factlist, rulehit = sp.runClips(tree.item(curItem).get("values"))
    global globalResult
    global globalFacts
    global globalHitRules
    globalResult = found
    globalFacts = factlist
    globalHitRules = rulehit
    if (not found) :
        redrawn_image = ip.getImageMatrices()
        redrawn_image = ImageTk.PhotoImage(Image.fromarray(redrawn_image).resize((500,500), Image.ANTIALIAS))
        shapeImageBox.configure(image=redrawn_image)
        shapeImageBox.photo = redrawn_image
    else:
        redrawn_image = ip.getRedrawnImage()
        redrawn_image = ImageTk.PhotoImage(Image.fromarray(redrawn_image).resize((500,500), Image.ANTIALIAS))
        shapeImageBox.configure(image=redrawn_image)
        shapeImageBox.photo = redrawn_image
    clearScrollText(matchedFacts)
    clearScrollText(detectionResult)
    clearScrollText(hitRules)
    printIntoScrollText(detectionResult,[str(found)])
    
def showFacts() :
    clearScrollText(matchedFacts)
    printIntoScrollText(matchedFacts, globalFacts)

def showHitRules() :
    clearScrollText(hitRules)
    printIntoScrollText(hitRules, globalHitRules)

def editRule() :
    os.system("cmd /c notepad clips.clp")

def startEditRuleThread(event) :
    edit_rule_thread = threading.Thread(target=editRule)
    edit_rule_thread.daemon = True
    edit_rule_thread.start()

# Window
window = Tk()
window.title("GUI Python AI")
window.geometry('1440x1080')

## Upper
sourceImageTitle = Label(window, text="Source Image", font=("Arial", 15))
sourceImageTitle.grid(column = 0, row = 0, padx = 125)

detectionImageTitle = Label(window, text="Detection Image", font=("Arial", 15))
detectionImageTitle.grid(column = 1, row = 0, padx = 55)

img = Image.open('source-image.png')
img = img.resize((500,500), Image.ANTIALIAS)
sourceImage = ImageTk.PhotoImage(img)
sourceImageBox = Label(window, image=sourceImage)
sourceImageBox.grid(column = 0, row = 1)

img = Image.open('shape-image.png')
img = img.resize((500,500), Image.ANTIALIAS)
shapeImage = ImageTk.PhotoImage(img)
shapeImageBox = Label(window, image=shapeImage)
shapeImageBox.grid(column = 1, row = 1)

#button frame
frame = Frame(window)
frame.grid(row=1,column=2) 
framebutton=Frame(frame)
framebutton.grid(column=0,row=1)

openImageButton = Button(framebutton, text="Open Image",command=sourceOnClick, width=25)
openImageButton.grid(column = 0, row = 1, pady = 12,padx=[0,10])

openRuleEditor = Button(framebutton, text="Open Rule Editor", command=lambda:startEditRuleThread(None), width=25)
openRuleEditor.grid(column = 0, row = 2, pady = 12,padx=[0,10])

showRules = Button(framebutton, text="Show Rules", command=showHitRules, width=25)
showRules.grid(column = 0, row = 3, pady = 12,padx=[0,10])

showFacts = Button(framebutton, text="Show Facts", command=showFacts, width=25)
showFacts.grid(column = 0, row =4, pady = 12,padx=[0,10])

texts = []

## Lower
detectionResultTitle = Label(window, text="Detection Result", font=("Arial", 15))
detectionResultTitle.grid(column = 0, row = 4, padx = 55)
detectionResult = scrolledtext.ScrolledText(window,width=40,height=20, state = tk.DISABLED)
printIntoScrollText(detectionResult, texts)
detectionResult.grid(column=0,row=5)
detectionResult.config(background="lightgrey")

matchedFactsTitle = Label(window, text="Matched Facts", font=("Arial", 15))
matchedFactsTitle.grid(column = 1, row = 4, padx = 55)
matchedFacts = scrolledtext.ScrolledText(window,width=40,height=20, state = tk.DISABLED)
printIntoScrollText(matchedFacts, texts)
matchedFacts.grid(column=1,row=5)
matchedFacts.config(background="lightgrey")

hitRulesTitle = Label(window, text="Hit Rules", font=("Arial", 15))
hitRulesTitle.grid(column = 2, row = 4, padx = 55)
hitRules = scrolledtext.ScrolledText(window,width=40,height=20, state = tk.DISABLED)
printIntoScrollText(hitRules, texts)
hitRules.grid(column=2,row=5)
hitRules.config(background="lightgrey")

#treeview component
tree=ttk.Treeview(frame)

tree.heading("#0",text="choose the shape",anchor="n")
tree.insert("" , 0,"allshape",   text="All Shapes")

id2 = tree.insert("allshape", 1, "triangle", text="triangle")
tree.insert(id2, "end", "segitiga_lancip", text="Segitiga lancip", values = "segitiga_lancip")
tree.insert(id2, "end", "segitiga_tumpul", text="Segitiga tumpul", values = "segitiga_tumpul")
tree.insert(id2, "end", "segitiga_siku_siku", text="Segitiga siku-siku", values = "segitiga_siku_siku")

id3=tree.insert(id2, 2, "segitiga_sama_kaki", text="Segitiga sama kaki", values = "segitiga_sama_kaki")
tree.insert(id3, "end", "segitiga_sama_kaki_siku", text="Segitiga sama kaki dan siku-siku", values = "segitiga_sama_kaki_siku")
tree.insert(id3, "end", "segitiga_sama_kaki_tumpul", text="Segitiga sama kaki dan tumpul", values = "segitiga_sama_kaki_tumpul")
tree.insert(id3, "end", "segitiga_sama_kaki_lancip", text="Segitiga sama kaki dan lancip", values = "segitiga_sama_kaki_lancip")

id4 = tree.insert("allshape",2, "segi_empat_tidak_beraturan", text="Segiempat tidak beraturan", values = "segi_empat_tidak_beraturan")

id5=tree.insert(id4, 2, "jajar_genjang", text="Jajaran Genjang", values = "jajar_genjang")
tree.insert(id5, "end", "segi_empat_beraturan", text="Segiempat beraturan", values = "segi_empat_beraturan")
tree.insert(id5, "end", "layang_layang", text="Segiempat berbentuk layang-layang", values = "layang_layang")

id6=tree.insert(id4, 2, "trapesium", text="Trapesium")
tree.insert(id6, "end", "trapesium_sama_kaki", text="Trapezium sama kaki", values = "trapesium_sama_kaki")
tree.insert(id6, "end", "trapesium_rata_kanan", text="Trapezium rata kanan", values = "trapesium_rata_kanan")
tree.insert(id6, "end", "trapesium_rata_kiri", text="Trapezium rata kiri", values = "trapesium_rata_kiri")

id7 = tree.insert("allshape", 3, "segi_lima_tidak_beraturan", text="Segi lima tidak beraturan", values = "segi_lima_tidak_beraturan")
tree.insert(id7, "end", "segi_lima_beraturan", text="segi lima sama sisi", values = "segi_lima_beraturan")

id8 = tree.insert("allshape", 4, "segi_enam_tidak_beraturan", text="Segi enam tidak beraturan", values = "segi_enam_tidak_beraturan")
tree.insert(id8, "end", "segi_enam_beraturan", text="segi enam sama sisi", values = "segi_enam_beraturan")

tree.bind('<Double-1>', selectItem)
tree.grid(row=1,column=1)

window.mainloop()