import cv2

class ImageProcessor :
  def __init__(self):
    pass

  def setImageFile(self, image_file) :
    self.image_file = image_file
    self.image_matrices = self.setImageMatrices()
    self.gray_image_matrices = self.setGrayImageMatrices()
    self.thresh = self.setThresh()
    self.contours = self.setContours()
    self.redrawn_image = self.setImageMatrices()
    self.redrawImage()

  def setImageMatrices(self) :
    return cv2.imread(self.image_file)

  def setGrayImageMatrices(self) :
    return cv2.cvtColor(self.image_matrices, cv2.COLOR_BGR2GRAY)

  def setThresh(self) :
    return cv2.threshold(self.gray_image_matrices, 150, 255, cv2.THRESH_BINARY)[1]

  def setContours(self) :
    return cv2.findContours(self.thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)[0]

  def getImageFile(self, image_file) :
    return self.image_file
    
  def getImageMatrices(self) :
    return self.image_matrices

  def getGrayImageMatrices(self) :
    return self.gray_image_matrices

  def getThresh(self) :
    return self.thresh

  def getContours(self) :
    return self.contours

  def getRedrawnImage(self) :
    return self.redrawn_image

  def showImage(self) :
    cv2.imshow(self.image_file, self.image_matrices)
    cv2.waitKey(0)

  def redrawImage(self) :
    cv2.drawContours(self.redrawn_image, [self.contours[1]], 0, (0,255,0), 3)

  def reset(self) :
    self.image_file = None
    self.image_matrices = None
    self.gray_image_matrices = None
    self.thresh = None
    self.contours = None