import cv2
from image_processor import ImageProcessor
import math
import numpy as np
from clips import Environment
import itertools
import re
def pixel2cm(pixel):
  return (pixel * 2.54 / 96)

class ShapeProcessor :
  def __init__(self):
    pass
  def new(self, img_processor : ImageProcessor) :
    self.processed_image = img_processor
    self.vertices = self.setVertices()
    self.angles = self.setAngles()
    self.magnitudeEdges = self.setMagnitudeEdges()
    self.facts = self.generateFacts()
    self.env = Environment()
    self.env.load("clips.clp")
  
  def reset(self):
    self.env.reset()
    self.env.clear()
    self.env.load("clips.clp")

  def setVertices(self) :
    self.vertices = np.array(list())
    contour = self.processed_image.getContours()[1]
    peri = cv2.arcLength(contour, True)
    vertices = cv2.approxPolyDP(contour, 0.01 * peri, True)
    return vertices

  def setAngles(self) :
    self.angles = np.array(list())
    angles = np.array(list())
    for i in range(0, self.vertices.shape[0]) :
      print()
      vectA = self.vertices[i] - self.vertices[(i-1)%self.vertices.shape[0]]
      vectB = self.vertices[i] - self.vertices[(i+1)%self.vertices.shape[0]]

      vectAcm = pixel2cm(vectA[0])
      vectBcm = pixel2cm(vectB[0])
      
      magnitudeVectA = pixel2cm(np.linalg.norm(vectA[0]))
      magnitudeVectB = pixel2cm(np.linalg.norm(vectB[0]))

      angleMagnitude = math.degrees(np.arccos(vectAcm.dot(vectBcm) / (magnitudeVectA*magnitudeVectB)))
      angles = np.append(angles, (angleMagnitude))
    return angles

  def setMagnitudeEdges(self) :
    self.magnitudeEdges = np.array(list())
    magnitudeEdges = np.array(list())
    edgeList = np.array(list())
    for i in range(0, self.vertices.shape[0]) :
      edge = self.vertices[i] - self.vertices[(i+1)%self.vertices.shape[0]]
      edgeList = np.append(edgeList, edge)
    for i in range(0, self.vertices.shape[0]) :
      magnitude = math.sqrt(pixel2cm(edgeList[2*i])**2 + pixel2cm(edgeList[2*i+1])**2)
      magnitudeEdges = np.append(magnitudeEdges, (magnitude))
    return magnitudeEdges

  def getVertices(self) :
    return self.vertices
  
  def getAngles(self) :
    return self.angles
  
  def generateFacts(self) :
    self.facts = []
    i=1
    for angle in self.angles:
      fact = "sudut "+str(i)+" "+str(angle)
      # print(fact)
      self.facts.append(fact)
      i=i+1
    for magnitude in self.magnitudeEdges:
      fact = "sisi "+str(i)+" "+str(magnitude)
      self.facts.append(fact)
      # print(fact)
      i=i+1
    fact = "banyaksudut " +str(self.vertices.shape[0])
    self.facts.append(fact)
    return self.facts
  
  def add_facts(self):
    for fact in self.facts:
      self.env.assert_string('('+fact+')')
  
  def runClips(self,method):
    self.env.reset()
    self.add_facts()
    answer = '(result '+method[0]+')'
    succeed = False
    rulelist = []
    factlist = []
    rulehit =  next(itertools.islice(self.env.activations(), 1))
    rulelist.append(str(rulehit).strip('0      '))
    while self.env.run(1):
      try:
        rulehit = next(itertools.islice(self.env.activations(), 1))
        rulelist.append(str(rulehit).strip('0      '))
      except Exception as e:
        print("empty")
    # print(rulelist)
    for fact in self.env.facts():
      factString = str(fact)
      factlist.append(factString[factString.find("(")+1:factString.find(")")])
      if(answer in factString):
        succeed = True
    return succeed, factlist ,rulelist